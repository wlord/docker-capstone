# Uses python 3.12 slim as a base image.
FROM python:3.12-slim

# Creates directory '/app' in the container to use as working directory.
WORKDIR /app

# Copy files in this folder into container's working directory.
COPY . /app

# Installs requirements.
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Exposes container port 80.
EXPOSE 80

# Runs the application.
CMD ["python", "main.py"]
