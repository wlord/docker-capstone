
from pymongo import MongoClient

client = MongoClient('mongodb://localhost:27017')
db = client['CapstoneDB']
col = db['capstone']

for x in col.find({}, {"_id":0}): 
    print(x)


# {'name': 'Pizza', 'price': 20, 'quantity': 125}
# {'name': 'Ice-Cream', 'price': 10, 'quantity': 234}
# {'name': 'Sushi', 'price': 15, 'quantity': 432}
# {'name': 'Soup', 'price': 17, 'quantity': 320}