from flask import Flask, render_template, request, redirect, url_for
from flask_wtf import CSRFProtect
from pymongo import MongoClient


# Startup flask.
app = Flask(__name__)
app.config['SECRET_KEY'] = 'b041becfa469cd0787dc7e1cd168d313'
csrf = CSRFProtect(app)

# Connect to MongoDB
client = MongoClient('mongodb://mongo:27017')
db = client['CapstoneDB']
col = db['capstone']


@app.route('/', methods=['GET', 'POST'])
def hello_world():

    # Display all items in the DB when a GET request is made.
    if request.method == 'GET':
        items = col.find({}, {"_id":0})
        return render_template("index.html", data=items) 
    
    # When form is submitted, retrieve data and add to the database, then update the page.
    elif request.method == 'POST':
        name = request.form['item-name']
        price = request.form['item-price']
        quantity = request.form['item-quantity']

        new_post = {'name': name, 'price': price, 'quantity': quantity}
        col.insert_one(new_post)

        return redirect('/')


# Runs the app.
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)